import java.util.*;
public class RouletteWheel
{
    private Random rand;
    private int lastSpin;

    public RouletteWheel()
    {
        rand = new Random();
        lastSpin  = 0;

    }
    public void spin()
    {
        this.lastSpin = rand.nextInt(37);
    }

    public int getValue()
    {

        return this.lastSpin;
    }
    public boolean hasWon(int playerBet)
    {
        if(playerBet == this.lastSpin)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}