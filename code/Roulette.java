import java.util.*;
public class Roulette
{
    public static void main(String[] args)
    {
        RouletteWheel rouletteWheel = new RouletteWheel();
        Scanner scan = new Scanner(System.in);
        Boolean play = true;
        String playerRequest;
        int playerMoney = 1000;
        int bet = 0;
        int houseNum;
        int playerBet = 0;
        if(playerMoney == 0)
        {
            play = false;
            System.out.println("Thank you for playing but you have run out of money.");
        }
        while(play)
        {
            play = false;
            System.out.println("Type in \"Y\" if you would like to place a bet. Press any other button to stop playing.");
            playerRequest = scan.next();
            if(playerRequest.equals("Y"))
            {
                while(!play)
                {
                    System.out.println("Please enter the number you want to place a bet on.");
                    playerBet = scan.nextInt();
                    if(playerBet <= 36 && playerBet >= 0)
                    {
                        play = true;
                    }
                }
            }
            else
            {
                int difference = playerMoney - 1000;
                if(difference > 0)
                {
                    System.out.println("Thank you for Playing you have gained a total of: " + difference + "$ in this session.");
                }
                else if(difference < 0)
                {
                    System.out.println("Thank you for playing you have lost a total of: " + difference + "$ in this session.");
                }
                else
                {
                    System.out.println("You havent won or lost anything. In this session.");
                }
            }

            if(play)
            {
                play = false;
                while(!play)
                {
                    System.out.println("How much money would you like to bet (Please be sure the ammount specifies is equal or less than what you have): ");
                    bet = scan.nextInt();
                    if(bet <= playerMoney)
                    {
                        play = true;
                    }
                }
                rouletteWheel.spin();
                houseNum = rouletteWheel.getValue();
                System.out.println("The roullette wheel landed on the number: " + houseNum);
                if(rouletteWheel.hasWon(playerBet))
                {
                    playerMoney = (bet * 35) + playerMoney;
                    System.out.println("Congratulations you won your new balance is: " + playerMoney);
                }
                else
                {
                    playerMoney = playerMoney - bet;
                    System.out.println("Oh no you lost your remaining balance is: " + playerMoney);
                }
            }
        }
    }


}